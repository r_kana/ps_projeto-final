Rails.application.routes.draw do
  resources :collaborations
  resources :participations
  resources :courses
  resources :selective_processes
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
