class SelectiveProcess < ApplicationRecord
    has_many :courses
    has_many :participations
end
