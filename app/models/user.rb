class User < ApplicationRecord
    has_many :participations
    has_many :collaborations
end
