class UserSerializer < ActiveModel::Serializer
  has_many :participations
  has_many :collaborations


  attributes :id, :name, :email, :password, :admin
end
