class CreateSelectiveProcesses < ActiveRecord::Migration[5.2]
  def change
    create_table :selective_processes do |t|
      t.integer :year
      t.integer :schoolTerm
      t.date :startDate
      t.date :finishDate
      t.date :registrationDeadline

      t.timestamps
    end
  end
end
